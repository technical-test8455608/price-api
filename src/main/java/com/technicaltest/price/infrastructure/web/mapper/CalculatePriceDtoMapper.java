package com.technicaltest.price.infrastructure.web.mapper;

import com.technicaltest.price.domain.model.Price;
import com.technicaltest.price.infrastructure.web.dto.CalculatePriceDto;

/**
 * The Class CalculatePriceDtoMapper.
 */
public class CalculatePriceDtoMapper {

	/**
	 * To dto.
	 *
	 * @param price the price
	 * @return the calculate price dto
	 */
	public static CalculatePriceDto toDto(Price price) {
		return CalculatePriceDto.builder()
				.productId(price.getProductId())
				.brandId(price.getBrandId())
				.rateApply(price.getPrice())
				.applicationDate(price.getStartDate())
				.finalPriceApply(price.getPrice())
				.build();
	}
}
