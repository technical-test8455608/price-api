package com.technicaltest.price.domain.enums;

/**
 * The Enum CurrencyEnum.
 */
public enum CurrencyEnum {
	
	/** The eur. */
	EUR
}
