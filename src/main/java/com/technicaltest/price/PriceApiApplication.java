package com.technicaltest.price;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The Class PriceApiApplication.
 */
@SpringBootApplication
public class PriceApiApplication {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(PriceApiApplication.class, args);
	}

}
