package com.technicaltest.price.infrastructure.web.mapper;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.technicaltest.price.domain.model.Price;

/**
 * The Class CalculatePriceDtoMapperTests.
 */
class CalculatePriceDtoMapperTests {

	/**
	 * To dto test.
	 */
	@Test
	void toDtoTest() {
		assertDoesNotThrow(() -> CalculatePriceDtoMapper.toDto(Price.builder().build()));
	}
}
