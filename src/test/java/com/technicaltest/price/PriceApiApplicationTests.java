package com.technicaltest.price;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * The Class PriceApiApplicationTests.
 */
@SpringBootTest
class PriceApiApplicationTests {

	/**
	 * Context loads.
	 */
	@Test
	void contextLoads() {
	}

}
